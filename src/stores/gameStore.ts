import {action, autorun, observable, toJS} from "mobx";
import axios from "axios";
import {User} from "./authStore";

const giphy = {
    baseURL: "https://api.giphy.com/v1/gifs/",
    apiKey: "sXpGFDGZs0Dv1mmNFvYaGUvYwKX0PWIh",
    tag: "cats",
    type: "random",
    rating: "pg-13"
};

let giphyURL = encodeURI(
    giphy.baseURL +
    giphy.type +
    "?api_key=" +
    giphy.apiKey +
    "&tag=" +
    giphy.tag +
    "&rating=" +
    giphy.rating
);


export class GameStore {
    @observable
    shuffleArr = [
        {
            status: true,
            activeStatus: false,
        },
        {
            status: false,
            activeStatus: false,
        },
        {
            status: false,
            activeStatus: false,
        }
    ];

    @observable
    userRate: any[] = [];

    @observable
    empty = true;

    @observable
    currentUser: User = {
        dataImage: [],
        password: "",
        username: ""
    };

    @observable
    canClick = true;

    @observable
    allActive = false;

    @action activeStatus = (index: number) => {
        autorun(() =>{
            this.checkNewUser();
        });
        if (this.empty) {
            if (this.shuffleArr[index].status) {
                this.shuffleArr[index].activeStatus = true;
                this.getImage();
                setTimeout(() => {
                    this.shuffleArr[index].activeStatus = false;
                    this.shuffle();
                }, 965);
            } else {
                this.allActive = !this.allActive;
                this.removeImage();
                setTimeout(() => {
                    this.allActive = !this.allActive;
                    this.shuffle();
                }, 965);
            }
            this.userRate = [];
        }

    };

    @action shuffle() {
        setTimeout(() => {
            this.shuffleArr = this.shuffleArr.slice().sort(() => Math.random() - 0.5);
            this.canClick = !this.canClick;
        }, 100);

    };

    @action setUserRate(element: any) {
        this.userRate = this.userRate.concat(element);
        autorun(() => {
            if (this.userRate.length > 0) {
                this.empty = true;
                this.canClick = true;
            } else if (this.currentUser.dataImage.length !== 0) {
                this.empty = false
            }
        });
    }

    @action checkNewUser(){

        if (this.currentUser.dataImage.length > 0){
            this.empty = true;
            this.canClick = true;
        }
        console.log(toJS(this))
    }

    @action getImage() {
        // eslint-disable-next-line
        for (let key in this.userRate) {
            axios.get(giphyURL).then(function (response: any) {
                const user = window.localStorage['authenticated'];
                const userDate = JSON.parse(window.localStorage[user]);
                userDate["dataImage"] = userDate.dataImage.concat([response.data.data]);
                window.localStorage[userDate.username] = JSON.stringify(userDate);
            })
        }
        if (this.userRate.length === 0 && this.currentUser.dataImage.length === 0) {
            axios.get(giphyURL).then(function (response: any) {
                const user = window.localStorage['authenticated'];
                const userDate = JSON.parse(window.localStorage[user]);
                userDate["dataImage"] = userDate.dataImage.concat([response.data.data]);
                window.localStorage[userDate.username] = JSON.stringify(userDate);
            });
        }

    }

    @action removeImage() {
        for (let key in this.userRate) {
            const user = window.localStorage['authenticated'];
            const userDate = JSON.parse(window.localStorage[user]);
            userDate["dataImage"] = userDate.dataImage.filter((elem: any) => {
                return elem.id !== this.userRate[key].id
            });
            window.localStorage[userDate.username] = JSON.stringify(userDate);
            this.checkNewUser()
        }
    }

    @action getCUser() {
        const user = window.localStorage['authenticated'];
        if (!!user) {
            this.currentUser = JSON.parse(window.localStorage[user]);
            this.checkNewUser();
        }
    }

}