import {action, observable} from "mobx";

export interface User {
    username: string;
    password: string;
    dataImage: [];
}

export class AuthStore {
    @observable
    loggedIn: boolean = false;
    @observable
    errors: string = "";

    @observable value = {
        username: '',
        password: '',
    };

    @action setUsername(username: string) {
        this.value.username = username;
    }

    @action setPassword(password: string) {
        this.value.password = password;
    }

    @action reset() {
        this.loggedIn = false;
        this.value.username = '';
        this.value.password = '';
        window.localStorage.removeItem('tstz.authenticated');
        window.localStorage.removeItem('authenticated');
    }

    @action signIn() {
        const user = JSON.parse(window.localStorage[this.value.username]);
        if (this.value.username  === user.username && this.value.password  === user.password){
            window.localStorage.setItem('tstz.authenticated', 'true');
            window.localStorage.setItem('authenticated', this.value.username);
            this.loggedIn = true;
            this.errors = ""
        } else {
            this.errors = "incorrect login or password"
        }
    }

    @action  checkAuthStatus = ()=> {
        if (localStorage.getItem('tstz.authenticated')) {
            const userD: User = JSON.parse(window.localStorage[window.localStorage['authenticated']]);
            this.value.username = userD.username;
            this.value.password = userD.password;

            this.loggedIn = true;
        } else {
            this.loggedIn = false
        }
    };

    @action signUp() {
        const users = window.localStorage[this.value.username];
        const data: User = {
            username: this.value.username,
            password: this.value.password,
            dataImage: []
        };
        if (!users) {
            window.localStorage[this.value.username] = JSON.stringify(data);
            window.localStorage.setItem('tstz.authenticated', 'true');
            window.localStorage.setItem('authenticated', this.value.username);
            this.loggedIn = true;
            this.errors = ""
        } else {
            this.loggedIn = false;
            this.errors = "this User Exist"
        }
    }

}


