import {action, observable} from "mobx";

class UserStore {
    @observable
    username: string | undefined;
    @observable
    password: string| undefined;
    @observable
    userData: [] | undefined;

    @action getData(){
        const user = window.localStorage['authenticated'];
        if (!!user) {
            const data = JSON.parse(window.localStorage[user]);
            this.username = data.username;
            this.password = data.password;
            this.userData = data.dataImage;
            // return data
        }
    }


}

export {UserStore}