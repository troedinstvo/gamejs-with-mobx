import React from "react";
import {AuthStore} from "../stores/authStore";
import {GameStore} from "../stores/gameStore";
import {UserStore} from "../stores/userStore";

export const storeContext = React.createContext({
    authStore: new AuthStore(),
    userStore: new UserStore(),
    gameStore: new GameStore(),
});