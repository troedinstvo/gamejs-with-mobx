import React from 'react';
import {observer} from "mobx-react";
import {useStores} from "../../hooks/use-stores";



export const Rate: React.FC = observer(()=> {
    const {gameStore} = useStores();

    return (
        <div className={"rate-container"}>
            <h4>Rate</h4>
            <div className="row" style={{minHeight: 150}}>
                {gameStore.userRate.length > 0 ?
                    <React.Fragment>
                        {gameStore.userRate.map((elem: any, key: number)=>
                            <div className="col z-depth-1" key={elem.id}>
                                <img src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150} />
                            </div>
                        )}
                    </React.Fragment>
                    : null
                }
            </div>
        </div>
    );
});