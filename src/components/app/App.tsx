import React from 'react';
import './App.scss';
import Header from "../header/header";
import {Switch, Route, BrowserRouter} from "react-router-dom";
import GameBoard from "../../pages/gameBoard";
import UserBoard from "../../pages/userBoard";
import SignIn from "../../pages/signIn";
import SignUp from "../../pages/signUp";
import ProtectedRoute from "../../hoc/protectedRoute";
import {observer} from "mobx-react";
import {useStores} from "../../hooks/use-stores";

const App: React.FC = observer(() => {
    const {authStore, userStore} = useStores();
    authStore.checkAuthStatus();
    if (authStore.loggedIn){
        userStore.getData();
    }
    // console.log(authStore.value.username);
    return (
        <BrowserRouter>
            <div className="App">
                <Header/>
                <Switch>
                    <ProtectedRoute exact path={"/"} component={GameBoard} />
                    <ProtectedRoute exact path={"/user-board"} component={UserBoard} />
                    <Route exact path={"/login"} component={SignIn} />
                    <Route exact path={"/register"} component={SignUp} />
                </Switch>
            </div>
        </BrowserRouter>
    );
});

export default App;
