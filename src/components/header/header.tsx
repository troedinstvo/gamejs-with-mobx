import React from 'react';
import logo from '../../logo.svg';
import {NavLink} from "react-router-dom";
import {observer} from "mobx-react";
import {useStores} from "../../hooks/use-stores";

const Header = observer(() => {
    const {authStore} = useStores();
    return (
        <header>
            <nav>
                <div className="nav-wrapper teal darken-3">
                    <NavLink to={"/"} className="brand-logo right"><img src={logo} className="App-logo" alt="logo" /></NavLink>
                    <ul id="nav-mobile" className="left hide-on-med-and-down">
                        <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}} to={"/"}>Game</NavLink></li>
                        <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}} to={"/user-board"}>User Board</NavLink></li>
                        {authStore.loggedIn?
                            <NavLink to={''} className="waves-effect waves-light btn" onClick={(event)=>{
                                event.preventDefault();
                                authStore.reset()
                            }}>Logout</NavLink>:
                            <React.Fragment>
                                <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}} to={"/login"}>Login</NavLink></li>
                                <li><NavLink exact activeStyle={{backgroundColor: "rgba(0,0,0,0.1)"}} to={"/register"}>Register</NavLink></li>
                            </React.Fragment>
                        }
                    </ul>
                </div>
            </nav>
        </header>
    );
});

export default Header;