import React from 'react';
import {observer} from "mobx-react";
import {useStores} from "../../hooks/use-stores";
import {toJS} from "mobx";

export const WonPictures: React.FC = observer(() => {
    const {gameStore} = useStores();
    // console.log(toJS(gameStore));
    const wPArr = gameStore.currentUser?.dataImage;

    return (
        <>
            <h4>Won Pictures</h4>
            <div className="wrap" style={{minHeight: 150}}>
                <div className="row">
                    {wPArr?.map((elem: any, key: number)=>(
                        <div className="col z-depth-1 hoverable" key={toJS(elem).id}
                        onClick={event => {
                            event.preventDefault();
                            gameStore.setUserRate(toJS(elem));
                            wPArr?.splice(key, 1)
                        }}
                        >
                            <img src={toJS(elem).images.downsized.url} alt={toJS(elem).title} width={"auto"} height={150} />
                        </div>
                    ))}

                </div>
            </div>
        </>
    );
});

// export default WonPictures;