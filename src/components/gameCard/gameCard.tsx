import React from 'react';
import {observer} from "mobx-react";
import {useStores} from "../../hooks/use-stores";

type GameCardProps = {
    card: {
        status: boolean,
        activeStatus: boolean
    },
    index: number
}

export const GameCard: React.FC<GameCardProps> = observer(({card,index})=> {
    const {gameStore} = useStores();
    return (
        <React.Fragment>
            <div className={card.activeStatus?"col s2 active-item hoverable":
                gameStore.allActive?"col s2 active-item hoverable":"col s2 hoverable"}
                 onClick={() => {
                     if(gameStore.canClick) {
                         gameStore.activeStatus(index);
                         gameStore.canClick = false;
                     }
                 }}
            >
                <p style={{
                    borderRadius:card.status? 8: 10,
                }}>{card.status ? "YOU WIN" : null}</p>
            </div>
        </React.Fragment>

    );
});

export default GameCard;