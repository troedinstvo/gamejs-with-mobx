import React from 'react';
import {observer} from "mobx-react";
import {Redirect} from "react-router-dom";
import {useStores} from "../hooks/use-stores";

const SignUp = observer(() => {
    const {authStore} = useStores();
    if (authStore.loggedIn){
        return <Redirect to={'/'}/>
    }
    return (
        <div className="container">
            <div className="row">
                <form
                    className="col s6 offset-s3"
                    style={{paddingTop: 15}}
                >
                    <h5>Register</h5>
                    <div className="row">
                        <div className="input-field col s12">
                            <input
                                id="last_name"
                                type="text"
                                className={authStore.errors.length > 0? "validate invalid":"validate"}
                                value={authStore.value.username}
                                onChange={(event => {
                                        authStore.setUsername(event.target.value)}
                                )}
                            />
                            <label htmlFor="last_name">Name</label>
                            <span className="helper-text" data-error={authStore.errors} data-success="right" />
                        </div>
                        <div className="input-field col s12">
                            <input
                                id="password"
                                type="password"
                                className={authStore.errors.length > 0? "validate invalid":"validate"}
                                value={authStore.value.password}
                                onChange={event => {
                                    authStore.setPassword(event.target.value)
                                }}
                            />
                            <label htmlFor="password">Password</label>
                            <span className="helper-text" data-error={authStore.errors} data-success="right" />
                        </div>
                    </div>
                    <button className="waves-effect waves-light btn" onClick={event => {
                        event.preventDefault();
                        authStore.signUp()
                    }}>Register</button>
                </form>
            </div>
        </div>
    );
});

export default SignUp;