import React from 'react';
import {observer} from "mobx-react";
import "../asset/gameCard.scss"
import {useStores} from "../hooks/use-stores";
import GameCard from "../components/gameCard/gameCard";
import {Rate} from "../components/rate/rate";
import {WonPictures} from "../components/wonPictures/wonPictures";

const GameBoard: React.FC = observer(() => {
    const {gameStore} = useStores();
    gameStore.getCUser();
    return (
        <div>
            <h2>Game Board</h2>
            <div className="container game-container">
                <div className="row">
                    {gameStore.shuffleArr.map((elem, key) =>
                        <GameCard card={elem} key={key} index={key}/>
                    )}
                </div>
            </div>
            <div className="container-fluid">
                <div className="row">
                    <Rate />
                </div>
                <div className="row">
                    <WonPictures/>
                </div>
            </div>
        </div>
    );
});

export default GameBoard;