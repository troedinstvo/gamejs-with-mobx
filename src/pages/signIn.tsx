import React from 'react';
import {observer} from "mobx-react";
import {useStores} from "../hooks/use-stores";
import {Redirect} from "react-router-dom";



const SignIn: React.FC = observer(() => {
    const {authStore} = useStores();
    if (authStore.loggedIn){
        return <Redirect to={'/'}/>
    }
    return (
        <div className="container">
            <div className="row">
                <form
                    className="col s6 offset-s3"
                    style={{paddingTop: 15}}
                >
                    <h5>Login</h5>
                    <div className="row">
                        <div className="input-field col s12">
                            <input
                                value={authStore.value.username}
                                id="last_name"
                                type="text"
                                className={authStore.errors.length > 0? "validate invalid":"validate"}
                                onChange={(event => {
                                    authStore.setUsername(event.target.value)}
                                    )}
                            />
                                <label htmlFor="last_name">Name</label>
                            <span className="helper-text" data-error={authStore.errors} data-success="right" />
                        </div>
                        <div className="input-field col s12">
                            <input
                                id="password"
                                type="password"
                                className={authStore.errors.length > 0? "validate invalid":"validate"}
                                value={authStore.value.password}
                                onChange={event => {
                                    authStore.setPassword(event.target.value)
                                }}
                            />
                            <label htmlFor="password">Password</label>
                            <span className="helper-text" data-error={authStore.errors} data-success="right" />
                        </div>
                    </div>
                    <button className="waves-effect waves-light btn" onClick={event => {
                        event.preventDefault();
                        authStore.signIn();
                    }}>Login</button>
                </form>
            </div>
        </div>
    );
});

export default SignIn;