import React from 'react';
import {observer} from "mobx-react";
import {useStores} from "../hooks/use-stores";

const UserBoard: React.FC = observer(() => {
const {userStore} = useStores();
userStore.getData();
// const userImage = userStore.userData;
    return (
        <div>
            <h2>User board</h2>
            <div className="row">
                {userStore.userData?.map((elem: any)=>(
                    <div className="col z-depth-1" key={elem.id}>
                        <img resource={"dsfsdf"} src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150} />
                    </div>
                ))}

            </div>
        </div>
    );
});

export default UserBoard;