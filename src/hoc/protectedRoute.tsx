import React from 'react';
import {observer} from "mobx-react";
import {useStores} from "../hooks/use-stores";
import {Route, Redirect} from "react-router-dom";


const ProtectedRoute = observer(({component: Component, ...rest}) => {
    const {authStore} =useStores();
    return (
        <Route {...rest} render={(location)=>
        authStore.loggedIn? <Component />: <Redirect to={"/login"}/>
        }/>
    );
});

export default ProtectedRoute;